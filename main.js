const buttonSearchIp = document.querySelector('.search-ip');

function getIp(url){
   return fetch(url)
    .then(response => response.json())
}

function createElement (parent, content){
        const li = document.createElement('li')
        li.innerText = content;
        parent.append(li)
}

buttonSearchIp.addEventListener('click', findLocation)

async function renderLocation(location){
    const {continent,country,regionName,city,district} = location;
    const locationList = document.createElement('ul');
    createElement (locationList, continent);
    createElement (locationList, country);
    createElement (locationList, regionName);
    createElement (locationList, city);
    createElement (locationList, district);
    console.log(continent,country,regionName,city,district);
    document.body.append(locationList);
}


async function findLocation(){
        const findeIp = await getIp('https://api.ipify.org/?format=json');
        const location = await getIp(`http://ip-api.com/json/${findeIp.ip}?fields=continent,country,regionName,city,district`);
        renderLocation(location)   
}
